import org.janusgraph.graphdb.database.management.ManagementSystem;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.*;
import org.janusgraph.core.schema.*;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.lang.Thread;
import javax.json.*;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
public class Main {

    static Map<String, Long> vertexMap = new ConcurrentHashMap<String,Long>();
    static Dispatcher dispatch = new Dispatcher();
    static Queue<Job> q = new ConcurrentLinkedQueue<Job>();
    static JanusGraph graph;
    static JanusGraphTransaction threadedGraph;
    static JanusGraphTransaction transaction;
    static final String  graph_path = "/home/cpaperman/janusgraph.properties";
    static GraphTraversalSource g;
    static final int THREAD_NUMBER = 20;
    static final int OPERATIONS_BEFORE_COMMIT = 10000;

    public static void init(){
	graph = JanusGraphFactory.open(graph_path);
	threadedGraph = graph.tx().createThreadedTx();
    } 
    public static void commit(){
	System.out.println("Commiting data ...");
	threadedGraph.tx().commit();
	System.out.println("... done");
	threadedGraph = graph.tx().createThreadedTx();

    }

    public static void addVertices(String name){

	q.add(Job.makeVertexJob(name));
    }
    public static void addEdges(String edge_description){
	String[] edge = edge_description.split(";");
	q.add(Job.makeEdgeJob(edge));
    }


    public static void loadAuthors(){
	try{
	    BufferedReader authors=new BufferedReader(new FileReader("/home/cpaperman/dblp_load/author.set"));
	    int i=0;
	    String line;
	    while ((line = authors.readLine()) != null)
		{
		addVertices(line);
		i++;
		if (i%1000 == 0){
		    System.out.print(i+"\r");
		}
		}
	}catch (IOException e){
	    e.printStackTrace();
	}
    }

    public static void loadEdges(){
	try{
	    System.out.println("parsing all edges");
	    BufferedReader authors=new BufferedReader(new FileReader("/home/cpaperman/dblp_load/author.edges"));
	    int i=0;
	    String line;
	    while ((line = authors.readLine()) != null) {
		addEdges(line);
		i++;
		if (i%1000 == 0)
		    System.out.print(i+"\r");
	    }
	    System.out.println("All edges parsed");

	} catch (IOException e){
	    e.printStackTrace();
	}
    }

    public static void main(String[] args) {
	dispatch = new Dispatcher();
	loadAuthors();
	dispatch.execute();
	System.out.println("Chargement noeuds fait");
	loadEdges();
	dispatch.execute();
	System.out.println("Chargement arêtes fait");
	System.exit(0);
    }

    private static class Job{		
	boolean isVertexJob;
	boolean isStopJob;
	String vertexName1;
	String vertexName2;
	String[] edgeDescription;
	private Job(boolean isVertexJob) {
	    this.isVertexJob = isVertexJob;
	    isStopJob = false;
	}

	static public Job makeVertexJob(String name) {
	    Job j = new Job(true);
	    j.vertexName1 = name;
	    return j;
	}

	public static Job makeStopJob() {
	    Job j = new Job(true);
	    j.isStopJob = true;
	    return j;
	}

	public static Job makeEdgeJob(String[] edge) {
	    Job j = new Job(false);
	    //j.edgeDescription = Arrays.copyOfRange(edge, 2, edge.length);
	    j.vertexName1 = edge[0];
	    j.vertexName2 = edge[1];
	    return j;
	}


	public void run(JanusGraphTransaction graphT) {		    
	    if(isVertexJob) {
		String[] arr = vertexName1.split(" ");
		String prefix ="";
		String name;
		prefix += arr[0];
		name = arr[arr.length-1];
		vertexMap.put(vertexName1, graphT.addVertex("prefix", prefix, "name", name).longId());
	    }
	    else {
		Vertex aut1 = graphT.getVertex(vertexMap.get(vertexName1));
		Vertex aut2 = graphT.getVertex(vertexMap.get(vertexName2));
		aut1.addEdge("coauthor", aut2);
		aut2.addEdge("coauthor", aut1);
	    }
	}

    }

    private static class Dispatcher{
	Thread[] threads;
	public Dispatcher() {
	    threads = new Thread[THREAD_NUMBER];					
	}

	public void execute() {
	    System.out.println("Dispatcher start");
	    for(int i = 0; i < THREAD_NUMBER; ++i) {
		Thread t = new JanusThread();
		threads[i] = t;
		t.start();
	    }
	    System.out.println("Threads launched");	    
	    for(int i = 0; i < THREAD_NUMBER; ++i) {
		try {
		    threads[i].join();
		}catch(Exception e) {e.printStackTrace();}
	    }
	    System.out.println("Dispatcher iteration over");
	}

    }

    private static class JanusThread extends Thread {
	public JanusThread() {
	    super(new Runnable() {
		    public void run() {
			JanusGraph  graph =  JanusGraphFactory.open(graph_path);
			JanusGraphTransaction graphTx = graph.newTransaction();
			String threadId = Long.toString(Thread.currentThread().getId());			
			int i = 0;
			double stop = OPERATIONS_BEFORE_COMMIT;
			Job j;
			while( (j = q.poll()) != null)
			    {
				j.run(graphTx);
				i += 1;
				if (i > stop)
				    {
					graphTx.commit();
					graphTx = graph.newTransaction();
					i = 0;
				    }
			    }
			
			graphTx.commit();
			graph.close();
		    }
		});
	}
    }

    private static class Chrono {
	long time; 
	boolean running;
	public Chrono() {
	    time = 0;
	    running = false;
	}

	public void start() {
	    time -= System.nanoTime();
	    running = true;
	}

	public void stop() {
	    time += System.nanoTime();
	    running = false;
	}
	public long getTime() {
	    return running ? time + System.nanoTime() : time;
	}

    }

}
