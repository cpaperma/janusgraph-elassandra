graph = JanusGraphFactory.open("../janusgraph.properties")
mgmt=graph.openManagement()
mgmt.makePropertyKey('name').dataType(String.class).make()
mgmt.makePropertyKey('prefix').dataType(String.class).make()
mgmt.makeEdgeLabel('proceedings').make()
mgmt.makeEdgeLabel('mastersthesis').make()
mgmt.makeEdgeLabel('inproceedings').make()
mgmt.makeEdgeLabel('phdthesis').make()
mgmt.makeEdgeLabel('incollection').make()
mgmt.makeEdgeLabel('www').make()
mgmt.makeEdgeLabel('article').make()
mgmt.makeEdgeLabel('book').make()
mgmt.makeEdgeLabel('coauthor').make()
mgmt.makePropertyKey('isbn').dataType(String.class).make()
mgmt.makePropertyKey('crossref').dataType(String.class).make()
mgmt.makePropertyKey('author').dataType(String.class).make()
mgmt.makePropertyKey('month').dataType(String.class).make()
mgmt.makePropertyKey('pages').dataType(String.class).make()
mgmt.makePropertyKey('booktitle').dataType(String.class).make()
mgmt.makePropertyKey('publisher').dataType(String.class).make()
mgmt.makePropertyKey('cdrom').dataType(String.class).make()
mgmt.makePropertyKey('editor').dataType(String.class).make()
mgmt.makePropertyKey('number').dataType(String.class).make()
mgmt.makePropertyKey('volume').dataType(String.class).make()
mgmt.makePropertyKey('address').dataType(String.class).make()
mgmt.makePropertyKey('title').dataType(String.class).make()
mgmt.makePropertyKey('year').dataType(String.class).make()
mgmt.makePropertyKey('cite').dataType(String.class).make()
mgmt.makePropertyKey('series').dataType(String.class).make()
mgmt.makePropertyKey('ee').dataType(String.class).make()
mgmt.makePropertyKey('journal').dataType(String.class).make()
mgmt.makePropertyKey('chapter').dataType(String.class).make()
mgmt.makePropertyKey('school').dataType(String.class).make()
mgmt.makePropertyKey('url').dataType(String.class).make()
mgmt.makePropertyKey('note').dataType(String.class).make()

mgmt.commit()
